# Phone flush - ssh connect to android filesystem

Simple way to access phone or any device with ssh server. 

- config file manager
- copies files one by one
- copies list of files
- removes files and list of files
- sends and runs bash scripts to remote device
- opens terminal 

Aimed to get camera and whatsapp media out of the phone and placed to file server.

![logo](./icons/phoneflush.png)

### Install 

[install ssh server app to phone ](https://play.google.com/store/apps/details?id=com.theolivetree.sshserver&hl=en) of any other what supports scp (all android ssh servers does not!)

clone from git 

    git clone https://ujoguru@bitbucket.org/ujoguru/phoneflush.git

install system wide

    ./install.sh proto

### Setup

asks first time device details

    gio.phoneflush my_phone  
    
login first time to terminal to finalize ssh settings

    gio.phoneflush my_phone -o


### Test

Outputs configuration data

    gio.phoneflush my_phone -t 1        

List some files in phone:

    gio.phoneflush my_phone -t 2

Tests 3-10 will copy some stuff from phone, 11 and 13 remove stuff from phone. 
*Be careful with empty config files, it might try to rm -rf /, not tested.!*

### Usage

    usage: phoneflush.py [-h] [-v] [-o] [-s SCRIPT] [-r RUN] [-d DELETE] [-t TEST_CASE] [-cmd CMD] [-ip IP]

    install ssh server to your phone from
    https://play.google.com/store/apps/details?id=com.theolivetree.sshserver&hl=en      
         
    positional arguments:
     
      device                device to connect with
      command               available commands: copy
      arguments             available  arguments: videos, images, wa-videos
      
    optional arguments:

      -o TERM, --terminal TERM            open ssh terminal
      -cmd CMD, --command CMD             run command at remote, return output
      -s SCRIPT, --send-script SCRIPT     send script to phone
      -r RUN, --run-script RUN            send script to phone
      -d DELETE, --delete-script DELETE   deletes script from phone 
      -t TEST_CASE, --test TEST_CASE      run test caseses
      -ip IP, --ip-address IP             remote ip address

      -h, --help                          show this help message and exit
      -v, --version                       show program's version number and exit

Send, run del script

    gio.phoneflush my_phone -s src/test.sh
    gio.phoneflush my_phone -r test.sh
    gio.phoneflush my_phone -d test.sh


### Benchmarks

TEST CASE #8 wa image directory (faster) 3924 items, 748,7 MB 

    time ./phoneflush.py honor1 -t8
    copying /storage/emulated/0/WhatsApp/Media/WhatsApp Images/*.jpg to /tmp/test2/3
    
- real	4m5,430s
- user	0m8,185s
- sys	0m8,696s

 TEST CASE #7 wa images by image (slow) 3924 items, 748,7 MB 
 
    time ./phoneflush.py honor1 -t7
    copying 3924 files /storage/emulated/0/WhatsApp/Media/WhatsApp Images/IMG-20151115-WA0000.jpg to /tmp/test2/3
    copying 3923 files /storage/emulated/0/WhatsApp/Media/WhatsApp Images/IMG-20151115-WA0001.jpg to /tmp/test2/3
    ...
    copying 2 files /storage/emulated/0/WhatsApp/Media/WhatsApp Images/IMG-20190222-WA0000.jpg to /tmp/test2/3
    copying 1 files /storage/emulated/0/WhatsApp/Media/WhatsApp Images/IMG-20190223-WA0000.jpg to /tmp/test2/3
    passed

- real	21m14,038s
- user	0m46,957s
- sys	0m43,866s

---- 
casa@ujo.guru 

---
