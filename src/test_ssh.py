import unittest
import os
import ssh

class TestSSH(unittest.TestCase):
	
	ssh = ssh.SSH("honor2")
	log = []

	def test_copy(self):		
		result = 0
		harvest = self.ssh.copy("/storage/emulated/0/DCIM/Camera", ['*.jpg'], "/tmp")			
		if harvest > 0:
			result = 1
		if str(type(harvest)) != "<class 'int'>": 			
			result = 1		
		self.assertEqual(result, 0)


	def test_get_fil(self):
		result = 0
		harvest = self.ssh.get_file_list("/storage/emulated/0/DCIM/Camera", "jpg")	
		if harvest == "":
			result = 1
		if str(type(harvest)) != "<class 'list'>": 		
			result = 1
		self.assertEqual(result, 0)

	def test_copy_di(self)	:
		result = 0
		harvest = self.ssh.copy_dir("/storage/emulated/0/DCIM", "jpg", "/tmp")
		if harvest > 0: 
			result = 1
		if str(type(harvest)) != "<class 'int'>": 
			result = 1
		self.assertEqual(result, 0)


	def test_send_script(self):

		result = 0
		file = open('/home/casa/.config/guru.io/test.sh', 'w')
		file.write("echo passed\n")
		file.close()
		
		harvest = self.ssh.send_script("/home/casa/.config/guru.io/test.sh/test.sh")	
		if harvest != "": 
			result = 1
		if str(type(harvest)) != "<class 'str'>": 
			result = 1
		self.assertEqual(result, 0)

	def test_run_script(self):

		result = 0
		harvest = self.ssh.run_script("test.sh").split(" ")
		if "passed" in harvest: 
			result = 1
		harvest = self.ssh.run_script("test.sh") 
		if str(type(harvest)) != "<class 'str'>": 
		 	result = 1
		self.assertEqual(result, 0)

	def test_run_cmd(self):

		result = 0
		harvest = self.ssh.run_cmd("echo passed")	
		if not "passed" in harvest : 
			result = 1
		if str(type(harvest)) != "<class 'str'>": 
			result = 1
		self.assertEqual(result, 0)

	def test_rm_list(self):

		result = 0
		harvest = self.ssh.rm_list("/storage/emulated/0", ['test.sh'], force = True)
		if harvest != 0: 
			result = 1
		if str(type(harvest)) != "<class 'int'>": 
			result = 1
		self.assertEqual(result, 0)

if __name__ == '__main__':
	unittest.main()
