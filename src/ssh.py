#!/usr/bin/python3
# -*- coding: utf-8 -*-

# Tälle luokalle kaksi eri testaustapaa: 
# unittest: python3 -m unittest test_ssh.py
# casastest: ./ssh.py

import os, sys
import configparser
import readline
import re
import config

class SSH():
	"ssh connection method for phoneflush"

	def __init__(self, remote_device, verbose = False):
				
		if verbose: print(self.__class__.__name__)
		self.config_location = "/home/casa/.config/guru.io/"
		self.config = config.Config(self.config_location+"ssh-"+remote_device+".cfg")

		self.remote_home = self.config.get_conf("ssh", "home")
		self.first_time = self.config.get_conf("ssh", "first_time")
		self.remote_ip = self.config.get_conf("ssh", "ip")
		self.remote_port = self.config.get_conf("ssh", "port")
		self.remote_keytype = self.config.get_conf("ssh", "keytype")		
		self.remote_password = self.config.get_conf("ssh", "password")


	def test1(self):
		return 0

	def open_terminal(self):
		"Opens ssh terminal to phone server"
		command = "sshpass -p "+self.remote_password+\
			" ssh -oHostKeyAlgorithms="+self.remote_keytype+\
			" -p "+self.remote_port+\
			" "+self.remote_ip
		return os.system(command)



	def copy(self, source, filelist, destination, verbose = False):			
		"Copies all speficied files on filelist"
		result=0
		for i in range(len(filelist)):			
			if verbose: print("copying "+str(len(filelist)-i)+" files " +source+"/"+filelist[i]+" to "+destination)				
			
			command = "sshpass -p "+self.remote_password+\
			" scp -oHostKeyAlgorithms="+self.remote_keytype+" -P "+self.remote_port+\
			" "+self.remote_ip+":'"+source+"'/"+filelist[i]+" "+destination
			result=result+os.system(command)
			if verbose: print(command)
			if verbose: print(result)
		return result



	def rm(self, location, target, force = False, verbose = False):
		"removes files"

		print("removing file" +location+"/"+target)	
		if force or input("are you sure? : [y/n]: ")=="y":
			command = ("sshpass -p "+ self.remote_password+\
				" ssh -p"+self.remote_port+\
				" -oHostKeyAlgorithms="+self.remote_keytype+\
				" "+self.remote_ip+\
				" rm -fr "+location+"/"+target+\
				" >/tmp/output")
			os.system(command)

			if verbose: print(command)
			if verbose: print(result)

		return open('/tmp/output', 'r').read().split()		



	def rm_list(self, location, filelist, force = False, verbose = False):			
		"removes all speficied files on filelist"
		result=0
		for i in range(len(filelist)):			

			if verbose: print("removing "+str(len(filelist)-i)+" files " +location+"/"+filelist[i])				
			if force or input("are you sure? : [y/n]: ")=="y":
		
				command = ("sshpass -p "+ self.remote_password+\
					" ssh -p"+self.remote_port+\
					" -oHostKeyAlgorithms="+self.remote_keytype+\
					" "+self.remote_ip+\
					" rm -fr "+location+"/"+filelist[i]+\
					" >/tmp/output")
				if verbose: print(command)
				result = result + os.system(command)
		
		if verbose: print(result)

		return result



	def copy_dir(self, source, filetype, destination, verbose = False):	
		"Copy all speficied filetype files from source"

		if verbose: print("copying "+source+"/"+"*."+filetype+" to "+destination)		

		command = "sshpass -p "+self.remote_password+\
			" scp -oHostKeyAlgorithms="+self.remote_keytype+" -P "+self.remote_port+\
			" "+self.remote_ip+":'"+source+"'/*."+filetype+" "+destination
		if verbose: print(command)
		return os.system(command)



	def send_script(self, script, verbose = False):
		"sends scipts to phone"		
		if verbose: print(script)
		command=("sshpass -p "+ self.remote_password+\
			" scp -P"+self.remote_port+\
			" -oHostKeyAlgorithms="+self.remote_keytype+\
			" "+script+" "+self.remote_ip+":"+self.remote_home+\
			" >/tmp/output")
		os.system(command)		

		if verbose: print(command)
		result = open('/tmp/output', 'r').read()
		return result 



	def run_script(self, script, verbose = False):
		"runs scipts in phone"
		command = ("sshpass -p "+ self.remote_password+\
			" ssh -p"+self.remote_port+\
			" -oHostKeyAlgorithms="+self.remote_keytype+\
			" "+self.remote_ip+\
			" sh "+self.remote_home+"/"+script+\
			" >/tmp/output")		
		if verbose: print(command)
		os.system(command)	
		return open('/tmp/output', 'r').read()



	def run_cmd(self, remote_command, verbose = False):
		"runs scipts in phone"
		command = ("sshpass -p "+ self.remote_password+\
			" ssh -p"+self.remote_port+\
			" -oHostKeyAlgorithms="+self.remote_keytype+\
			" "+self.remote_ip+\
			" '"+str(remote_command)+"'"\
			" >/tmp/output")		
		if verbose: print(command)
		os.system(command)	
		return open('/tmp/output', 'r').read()



	def get_file_list(self, location, file_format, verbose = False):
		"Input fileformat. Collects an list of all camera files in phone"
		command = "sshpass -p "+self.remote_password+\
			" ssh -oHostKeyAlgorithms="+self.remote_keytype+\
			" -p "+self.remote_port+\
			" "+self.remote_ip+\
			" ls "+location +" |grep ."+file_format+\
			" >/tmp/output"
		if verbose: print(command)
		os.system(command)		
		return open('/tmp/output', 'r').read().split()	


if __name__ == "__main__":

	import subprocess
	ssh = SSH("honor2", verbose = False)
	fail_count = 0
	warning_count = 0



	# paikallisen testitapauksen template

	# harvest = 			
	# if harvest > 0:
	# 	print(ssh.copy.__name__+" failed")
	# 	fail_count += 1 	
	# if str(type(harvest)) != "<class 'int'>": 
	# 	print(ssh.get_file_list.__name__+" output type warning "+str(type(harvest)))	
	# 	warning_count += 1	

	
	harvest = ssh.copy("/storage/emulated/0/DCIM/Camera", ['IMG_20190226_043220.jpg'], "/tmp", verbose = False)			
	if harvest > 0:
		print(ssh.copy.__name__+" failed")
		fail_count += 1 	
	if str(type(harvest)) != "<class 'int'>": 
		print(ssh.copy.__name__+" output type warning "+str(type(harvest)))	
		warning_count += 1	
		
	harvest = ssh.get_file_list("/storage/emulated/0/DCIM/Camera", "jpg", verbose = False)	
	if harvest == "":
		print(ssh.get_file_list.__name__+" failed")	
		fail_count += 1
	if str(type(harvest)) != "<class 'list'>": 		
		print(ssh.get_file_list.__name__+" output type warning "+str(type(harvest)))	
		warning_count += 1
	
	harvest = ssh.copy_dir("/storage/emulated/0/DCIM", "jpg", "/tmp", verbose = False)
	if harvest > 0: 
		print(ssh.copy_dir.__name__+" failed")	
		fail_count += 1 
	if str(type(harvest)) != "<class 'int'>": 
		print(ssh.copy_dir.__name__+" output type warning "+str(type(harvest)))	
		warning_count += 1

	file = open('/home/casa/.config/guru.io/test.sh', 'w')
	file.write("echo passed\n")
	file.close()
	subprocess.call(['chmod', '0755', '/home/casa/.config/guru.io/test.sh'])	
	harvest = ssh.send_script("/home/casa/.config/guru.io/test.sh", verbose = False)	
	if harvest != "": 
		print(ssh.send_script.__name__+" failed")	
		fail_count += 1 
	if str(type(harvest)) != "<class 'str'>": 
		print(ssh.send_script.__name__+" output type warning "+str(type(harvest)))	
		warning_count += 1

	harvest = ssh.run_script("test.sh", verbose = False)	
	if "passed" in harvest.split(" "): 
		print(ssh.run_script.__name__+" failed")	
		fail_count += 1 
	if str(type(harvest)) != "<class 'str'>": 
		print(ssh.run_script.__name__+" output type warning "+str(type(harvest)))	
		warning_count += 1

	harvest = ssh.run_cmd("echo passed", verbose = False)	
	if not "passed" in harvest : 
		print(ssh.run_cmd.__name__+" failed")	
		fail_count += 1 
	if str(type(harvest)) != "<class 'str'>": 
		print(ssh.run_cmd.__name__+" output type warning "+str(type(harvest)))	
		warning_count += 1

	harvest = ssh.rm_list("/storage/emulated/0", ['test.sh'], verbose = False, force = True)
	if harvest != 0: 
		print(ssh.rm_list.__name__+" failed")	
		fail_count += 1 
	if str(type(harvest)) != "<class 'int'>": 
		print(ssh.rm_list.__name__+" output type warning "+str(type(harvest)))	
		warning_count += 1

	if fail_count > 0:
		print(__file__+" fails: "+str(fail_count))
		exit(1)

	if warning_count > 0:
		print(__file__+" warnings: "+str(warning_count))
		exit(2)
	exit(0)
	



