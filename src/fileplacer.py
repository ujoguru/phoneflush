#!/usr/bin/python3
# -*- coding: utf-8 -*-
import os, sys
import readline, datetime
import datetime
import config
import ssh 
import random

class FilePlacer():
	"plases remote files to correlating tag to local filesystem "

	all_files = []
	copied_files = []	
	new_files = []
	old_files = []


	def __init__(self, user_name, remote_device, remote_type, verbose = False):
		
		
		if verbose: print(self.__class__.__name__)
		self.date = datetime.date.today()
		self.config_location = "/home/casa/.config/guru.io/"
		
		# self.temp_location = "/tmp/files-"+self.date.strftime("%s")
		# if not os.path.exists(self.temp_location): 
		# 	os.makedirs(self.temp_location)		
		#self.android_dev = android.Android(remote_device) 				TODO toinen yhteystyyppi tuntuu luonnolliselta esiellä tässä
		self.ssh = ssh.SSH(remote_device, verbose = verbose)
		self.remote = config.Config(self.config_location+"remote-"+remote_type+".cfg", verbose = verbose)  
		self.remote_home = self.remote.get_conf("remote","home")
		
		self.local = config.Config(self.config_location+"places-"+user_name+".cfg", verbose = verbose)
		self.local_home = os.environ['HOME']		
		



	def get_remote_folder_for_tag(self, value_list, tag_depth = 0, verbose = False): 	
		"Find in tags in option tag values. Returns folder location"

		sections = self.remote.list_sections()
		if verbose: print(sections)
		for i in range(1,len(sections)):	
			tags = self.remote.get_conf(str(sections[i]),"tag")
			if verbose: print(str(value_list)+":"+tags)
			if str(value_list) in tags:			 				
				break
		result = self.remote.get_conf(str(sections[i]),"location")
		if verbose: print(result)
		return result

	def get_local_folder_for_tag(self, value_list, tag_depth = 0, verbose = False):
		"Input value, option, tag_depth: find in tags, returns number folder where tag is found. tag_depth is tag's position on list"

		sections = self.local.list_sections()
		for i in range(1,len(sections)):	
			tags = self.local.get_conf(str(sections[i]),"tag")
			#print(str(value_list)+":"+tags)
			if str(value_list) in tags:			 				
				break
		result = self.local.get_conf(str(sections[i]),"location")
		return result

	def get_local_data_for_tag(self, value_list, tag_depth = 0, verbose = False):
		"value, option, tag_depth: find in tags, returns array: [[ id, tag, location, subfolser],['file1','file2'] "
		result = [[],[]]
		sections = self.local.list_sections()
		for i in range(1,len(sections)):	
			tags = self.local.get_conf(str(sections[i]),"tag")
			#print(str(value_list)+":"+tags)
			if str(value_list) in tags:			 				
				break
		result[0].append(sections[i])
		result[0].append(self.local.get_conf(str(sections[i]),"tag"))
		result[0].append(self.local.get_conf(str(sections[i]),"location"))		
		result[0].append(self.local.get_conf(str(sections[i]),"subfolder"))	

		if verbose: print(result)						
		return result


	def get_remote_data_for_tag(self, value_list, tag_depth = 0, verbose = False):
		"value, option, tag_depth: find in tags, returns array: [[ id, tag, location, subfolser],['file1','file2'] "
		result = [[],[]]
		sections = self.remote.list_sections()
		for i in range(1,len(sections)):	
			tags = self.remote.get_conf(str(sections[i]),"tag")
			#print(str(value_list)+":"+tags)
			if str(value_list) in tags:			 				
				break
		result[0].append(sections[i])
		result[0].append(self.remote.get_conf(str(sections[i]),"tag"))
		result[0].append(self.remote.get_conf(str(sections[i]),"location"))		
		result[0].append(self.remote.get_conf(str(sections[i]),"file"))	
		if verbose: print(result)						
		return result


	def list_tags(self, sections, tag_depth = 0, verbose = False):
		"value, option, tag_depth: returns list of tags"
		tags = []
		for i in range(1,len(sections)):	
			tags.append(self.local.get_conf(str(sections[i]),"tag"))			
		return tags

def test():

	test = FilePlacer("casa", "honor2", "android")

	print("Test 1")
	
	remote_tags = test.list_tags(test.remote.list_sections())	
	local_tags = test.list_tags(test.local.list_sections())	
	tag = random.choice(local_tags)
	expected = test.get_remote_data_for_tag(tag)[0][1]
	output = test.get_local_data_for_tag(tag)[0][1]
	
	
	print("test_tag: "+str(tag))	
	print("expected: "+str(expected.split(",")[0]))
	print("returned: "+str(output.split(",")[0]))


	if output in expected: print("passed")
	else: print("failed")







if __name__ == "__main__":

	test()
