#!/usr/bin/python3
 # -*- coding: utf-8 -*-
import os, sys
import configparser
import readline
import re
		
class Config:
	"General class for configurations"

	def __init__(self, config_file, verbose = False):
		"constructs local configuration data"

		self.config_file = config_file
		self.verbose = verbose
		if verbose: print(self.__class__.__name__, config_file)
				
		if not os.path.isfile(self.config_file): 
			print("config file :"+self.config_file+" missing")
			#self.add_config(self.config_file)
			exit(1)
		
		self.config = configparser.RawConfigParser()		
		self.config.read(self.config_file)

	
	def section_exist(self, section):
		return self.config.has_section(section)


	def option_exist(self, section, option):
		return self.config.has_option(section, option)


	def list_sections(self):
		return self.config.sections()


	def list_options(self, section):	
		if self.config.has_section(section):
			return self.config.items(section)
		print("section not exist")
		return False

	# def list_values(self, section):	
	# 	if self.config.has_section(section):
	# 		return self.config.items(section)
	# 	print("section not exist")
	# 	return False

	def get_conf(self, section, option):		
		"Get configuration, input sectiton and option."	
		if self.config.has_option(section, option):				
			return self.config.get(section, option)
		else:
			if self.verbose: print("section '"+section+":"+option+"' not exist")
			return False

	def set_conf(self, section, option, value, force = False):
		"Set configuration, input sectiton, option and value."	
		if not self.config.has_section(section):
			if input(section+": not exist, create? [y/n]") != "y": 
				return False

			self.config.add_section(section)

		if self.config.has_option(section, option) and not force:
			if input(section+":"+option+" exist, overwrite? [y/n]") != "y": 
				return False

		self.config.set(section, option, value)
		return True


	def rem_option(self, section, option, force = False):
		"Removes configuration, input sectiton and option. Force option removes confirmation questions."		
		if self.config.has_option(section, option) and not force:
			if input(section+":"+option+" exist, remove? [y/n]") != "y": 
				return False
			
		self.config.remove_option(section, option)
		return True

	def rem_section(self, section, force = False):
		"Removes whole section of configurations. Input section. Force option removes confirmation questions."		
		if self.config.has_section(section):
			if input(section+": exist, remove? [y/n]") != "y": 
				return False
			self.config.remove_section(section)

	def save_config_file(self, force = False):		
		"Saves configurations to file."		
		if os.path.isfile(self.config_file) and not force:
			if input(self.config_file+" file exist, overwrite? [y/n]") != "y":
				return False

		with open(self.config_file, 'w') as to_file:		    	
			self.config.write(to_file)
			to_file.close()

		return True

	def read_config_file(self):				
		"Reads the configurations file"		
		self.config.read(self.config_file)			

