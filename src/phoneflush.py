#!/usr/bin/python3
 # -*- coding: utf-8 -*-
release_version = "0.3.0"
# phoneflush TODO - bash is too complicated to update, write same with python!

import os
from argparse import ArgumentParser
import fileplacer as remote

def run_command(command, argument): 	
	"Listallinen prototyyppejä. Alkuperäisen viritysversion jättänyt manuaaliseksi testeriksi, poistanut sitä mukaa kun vanhenevat"

	if command == "cp":
		remote_dir = device.get_remote_folder_for_tag(argument, verbose = args.verbose)	
		local_dir = device.get_local_folder_for_tag(argument, verbose = args.verbose)
		print(remote_dir, "jpg", local_dir)
		#device.ssh.copy_dir(remote_dir,"jpg", local_dir)

	if command == "test": 
		test_case = int(argument)
		
		if test_case == 1:
			pass
			# print("TEST CASE #"+str(test_case)+" basic setups")
			# test_data = device.list_tags(device.)
			# for i in range(0,len(test_data)):
			# 	print(device.get_remote_folder_for_tag(str(test_data[i]))+" -> "+str(test_data[i])+" -> "+device.get_local_folder_for_tag(str(test_data[i])))
			
		elif test_case == 2:
			print("TEST CASE #"+str(test_case)+" ")

		elif test_case == 3:
			result = 0
			print("TEST CASE #"+str(test_case)+" send script and run")		
			device.ssh.send_script("src/test.sh")
			result = device.ssh.run_script("test.sh")
			device.ssh.rm(device.remote_home,"test.sh", force = True)
			print(result)
			pass

		elif test_case == 4:			
			result = 0
			print("TEST CASE #"+str(test_case)+" ")		
			if result: print("passed")

		elif test_case == 5:
			print("TEST CASE #"+str(test_case)+" ")
			remote_dir = device.get_remote_folder_for_tag("photo")
			local_dir = device.get_local_folder_for_tag("photo")
			print(remote_dir, "jpg", local_dir)
			device.ssh.copy_dir(remote_dir,"jpg", local_dir)

		elif test_case == 6:
			print("TEST CASE #"+str(test_case)+" ")
			#[[id,tag,location,subfolser],["file",s]
			test_data = device.get_remote_folder_for_tag("photo")
			#print(test_data)
			result = device.ssh.get_file_list(test_data,"jpg", verbose = args.verbose)
			if result: print("passed")

		elif test_case == 7:
			print("TEST CASE #"+str(test_case)+" ")
			tag = "photo"
			remote_data = device.get_remote_data_for_tag(tag, verbose = args.verbose)						
			remote_location = remote_data[0][2] 											# TODO tän listamallin käsittelystä korkeamman tason tapa käsitellä tiedostolistoja
			files = remote_data[0][3]
			tags = remote_data[0][1]
			file_list = device.ssh.get_file_list(remote_location, files, verbose = args.verbose)
			local_data = device.get_local_data_for_tag(tag, verbose = args.verbose)			
			local_location = local_data[0][2]			
			result = device.ssh.copy(remote_location, file_list, local_location)
			if result == 0: print("passed")

		elif test_case == 8:
			print("TEST CASE #"+str(test_case)+" ")				
			result=""	
			if result: print("passed")

		elif test_case == 9:
			print("TEST CASE #"+str(test_case)+" ")				
			result=""	
			if result: print("passed")

		elif test_case == 10:
			print("TEST CASE #"+str(test_case)+" ")				
			result=""	
			if result: print("passed")

		else:
			print("no such test case")
			exit(1)
		exit(0)

argparser = ArgumentParser(description="install ssh server to your phone from https://play.google.com/store/apps/details?id=com.theolivetree.sshserver&hl=en")
argparser.add_argument("device", default = "honor1", help=", device to connect with")
argparser.add_argument("command", default = 0, help="available commands: copy")
argparser.add_argument("argument", default = 0, help="available  arguments: videos, images, wa-videos")
argparser.add_argument("-ver", "--version", 	action = 'version', version = "%(prog)s "+release_version)
argparser.add_argument("-v", "--verbose",	action='store_true', dest = "verbose", help = "verbose mode")
argparser.add_argument("-o", "--terminal",	action='store_true', dest = "term", help = "open ssh terminal")
#argparser.add_argument("-c", "--copy",	dest = "copy", default = 0, help = "copes files from phone: source")
argparser.add_argument("-s", "--send-script",	dest = "script", default = 0, help = "send script to phone")
argparser.add_argument("-r", "--run-script",	dest = "run", default = 0, help = "send script to phone")
argparser.add_argument("-d", "--delete-script",	dest = "delete", default = 0, help = "deletes script from phone")
argparser.add_argument("-t", "--test",		dest = "test_case", default = 0, help = "run test caseses")
argparser.add_argument("-cmd", "--command",	dest = "cmd", default = 0, help = "run command at remote, return output")
argparser.add_argument("-ip", "--ip-address",		dest = "ip", default = 0, help = "remote ip address")

args = argparser.parse_args()
device = remote.FilePlacer("casa", "honor2", "android",verbose = args.verbose)

if int(args.test_case) > 0: test(int(args.test_case))
if args.ip != 0: device.ip = args.ip
if args.term: device.ssh.open_terminal()
if args.script != 0: device.ssh.send_script(args.script)
if args.run != 0: print(device.ssh.run_script(args.run))
if args.delete != 0: device.ssh.rm_list(device.remote_home,args.delete, force = True)
if args.cmd != 0: print(device.run_cmd(args.cmd))
if args.command != 0: print(run_command(args.command,args.argument))
#if args.copy != 0: print(run_command(args.command,args.argument))
