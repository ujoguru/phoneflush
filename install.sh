# Phoneflush installer for poc and proto - debian linux only

# Requires
tree >/dev/null || sudo apt install tree
sshpass >/dev/null || sudo apt install sshpass
pip --version >/dev/null || sudo apt install pip

# Config files 
cfg_location="$HOME/.config/guru.io"
echo $cfg_location
[[ -d $cfg_location ]] || mkdir -p $cfg_location 

cp cfg/places-USER.cfg "$cfg_location"
cp cfg/phone-DEVICE.cfg "$cfg_location"
cp cfg/ssh-android.cfg "$cfg_location"
sudo cp ./src/phoneflush.py /usr/bin/gio.phoneflush
pip3 install -r requirements.txt
echo "#### Alpha note #### - rename places and  phone .cfg by replasing USER to our your nick and DEVICE you phone name at $cfg_location"
echo "####                 - edit phone-DEVICE.cfg "


